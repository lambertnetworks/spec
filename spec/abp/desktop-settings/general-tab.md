## General tab
### General tab default
![](/res/abp/desktop-settings/general-tab.jpg)

1. [General tab headline](#general-tab-headline)
1. [General tab description](#general-tab-description)
1. [Recommended filters section](#recommended-filters-section)
1. [Acceptable Ads section](#acceptable-ads-section)
1. [Language section](#language-section)
1. [Error state](#error-state)

#### General tab headline
`General`

Navigation label will match headline.

#### General tab description
`Determine what Adblock Plus shows and hides on websites`

### Recommended filters section
Design: https://eyeogmbh.invisionapp.com/public/share/YA1CD4VKQB

1. [Recommended filters section headline](#recommended-filters-section-headline)
1. [Recommended filter lists](#list-of-recommended-filter-lists)
1. [Tooltip icon](#tooltip-icon)
1. [Tooltip pop-up](#tooltip-pop-up)

#### Recommended filters section headline
 `RECOMMENDED FILTERS`

#### List of recommended filter lists
Checkbox to install/uninstall each filter list. All filter lists are uninstalled by default.

**Detail:** The filter lists are sorted alphabetically by item title.

| Filter list name | Filter list title | Tooltip | Filter list URL | Notes |
|-----------|---------------|---------------|--------------|--------------|
| EasyPrivacy | `Block additional tracking` | `Protect your privacy from known entities that may track your online activity across websites you visit.` | https://easylist-downloads.adblockplus.org/easyprivacy.txt |
| I Don't Care About Cookies | `Block cookie warnings` | `By adding this filter you are waiving your right to be notified about cookies and edit cookie settings on individual websites. Please note that some websites may track you by default. More info here: https://adblockplus.org/block-cookie-warnings` [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=block_cookie_warnings`| https://www.i-dont-care-about-cookies.eu/abp/ | Hidden for FR and DE locales |
| Fanboy's Notifications | `Block push notifications` | `Stop websites from asking you to allow push notifications that could track your online activity.` | https://easylist-downloads.adblockplus.org/fanboy-notifications.txt |
| Fanboy's Annoyances | `Block social media icon tracking` | `The social media icons on the websites you visit allow social media networks to build a profile of you based on your browsing habits - even when you don’t click on them. Hiding these icons can protect your profile.` | https://easylist-downloads.adblockplus.org/fanboy-annoyance.txt |

#### Tooltip icon

Tooltip is triggered when users click on the `?` icon, clicking on `X` or outside of the pop-up closes the tooltip.

#### Tooltip pop-up

The tooltip will open in the direction that has the most vertical space available in the current viewport.

The tooltip pop-up should not extend beyond 12.5em in height. If the text overflows beyond this height, then add a scroll bar in.

Refer to the table in [Recommended filter lists](#recommended-filter-lists) for tooltip descriptions. 

### Acceptable Ads section
![](/res/abp/desktop-settings/general-default-acceptable-ads.jpg)

|Section|Content|Behavior|
|-------|-------|--------|
|1. Acceptable Ads section headline|`ACCEPTABLE ADS`| |
|2. Acceptable Ads section description|`Acceptable ads are non-annoying ads that comply with a strict ad standard. They help generate revenue for content creators and do not interfere with the content you are viewing.`| |
|3. Acceptable Ads options|`Show acceptable ads` |Checked by default.|
|3a. Acceptable Ads options Description |`Acceptable ads do not interfere with the content you are viewing. Learn more [1]` |[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=acceptable_ads_criteria` in a new tab|


#### Acceptable Ads opt-out Survey

**Design:** 
https://scene.zeplin.io/project/5bb73eebd31f1e2e9818f62a/screen/5cc33d52fca2322d1906b80a

*  When a user opts-out from Acceptable Ads, a tooltip will shown with the following text: `To help us improve Adblock Plus, mind sharing why you’ve turned off Acceptable Ads?`
*  `GO TO THE SURVEY` -  `%LINK%=acceptable_ads_survey` - This will open in new tab
*  `NO, THANKS` - This will dismiss the message. Do NOT dismiss the message if user clicked somewhere else or moved to another Settings section. The message should be dismissed only by clicking on `NO, THANKS`.

##### Only allow ads without third-party tracking
This appears as a subset of the `Acceptable Ads` options, which enables the Privacy-friendly Acceptable Ads filter list. 

This option is only active when `Show acceptable ads` is selected, otherwise it will be inactive with reduced opacity.

###### Title:
`Only allow ads without third-party tracking`

###### Description:
`[Learn more][1]`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=privacy_friendly_ads` in a new tab.

##### Do not track conditions
![](/res/abp/desktop-settings/general-default-acceptable-ads-dnt.jpg)

If a user selects `Only allow ads without third-party tracking` AND has DNT disabled, display the below text within the table:

`**Note:** You have **Do Not Track (DNT)** disabled in your browser settings. For this feature to work properly, please enable **DNT** in your browser preferences. [Find out how to enable DNT][1] and redirect to the official browser instructions for enabling DNT)`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=(adblock_plus_chrome_dnt|adblock_plus_firefox_dnt|adblock_plus_opera_dnt|adblock_plus_edge_dnt)` depending on which browser the extension is running in a new tab.

#### Acceptable Ads notification

![](/res/abp/desktop-settings/general-default-acceptable-ads-message.jpg)

If a user selects `Block additional tracking` (EasyPrivacy) and `Show acceptable ads`, then show the above notification within the page. This will continue showing until the user actively clicks to close the message. 

Clicking X or `OK, got it` closes the notification.

##### Notification text

```
We noticed you have both **Block additional tracking** and **Show acceptable ads** enabled.

We want you to know that in order for advertisers to show you more relevant ads, there *may* be some tracking with acceptable ads.

If you prefer extra privacy, select the **Only allow ads without third-party tracking** checkbox below.

[OK, got it](closes notification)
```
### Language section

#### Filter List Activation Feature

![](/res/abp/desktop-settings/general-default-language.jpg)

1. `LANGUAGE FILTER LISTS`
1. [Language section description](#language-section-description)
1. [Default language](#default-language)
1. [Change language](#change-language)
1. [Add language](#add-language)
1. `**TIP:** Only select the languages you need. Selecting more will slow down the ad blocker and, therefore, your browsing speed. `

##### Language section description
`Add or remove languages that you want Adblock Plus to block ads in. [Learn more][1]`

[1]: Opens [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=language_subscription` in a new tab.

##### Default language

Language filter list selected based on the browser's default language. Bundled filter list labels are displayed as follows:

"*language of filter list* + English" [font color: ![#4A4A4A](https://placehold.it/15/4A4A4A/000000?text=+) `#4A4A4A`] "(*Filter list name*)" [font color: ![#BBB](https://placehold.it/15/BBB/000000?text=+) `#BBB`]

If it is an unbundled filter list, then hide `+ English`.

To see all available language options and corresponding filter lists go to  [Language filter lists](/spec/abp/filter-lists.md#language-filter-lists).

##### Change language

###### Label
`CHANGE`

###### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *change the language filter subscription*. 

##### Add language
###### Label
`+ ADD A LANGUAGE FILTER LIST`
 
###### Behaviour
Button to trigger the [Language dropdown](#language-drop-down).

Clicking on a language within the dropdown will automatically close the window and *add the language filter subscription*. 

### Language drop down
![](/res/abp/desktop-settings/general-default-language-drop-down.jpg)

1. [Drop down](#drop-down)
1. [Selected language](#selected-language)
1. Hover state
1. `SELECT A LANGUAGE` 
1. [Scroll bar](#scroll-bar)

#### Drop down

The bundled language filter subscriptions are always used (i.e. it includes EasyList), unless it is EasyList, which is available unbundled. 

Display labels in the drop down as "*language of filter list* + English", unless it is EasyList, then only display `English`.

Clicking anywhere outside of the drop down, or on a [selected language](#selected-language) closes the drop down.

#### Selected language

Already added filter subscriptions will appear greyed out and disabled in the drop down.

#### Scroll bar

The size of the layover menu should correspond to the screen size. The scroll bar should adjust accordingly. 

#### Multiple languages
![](/res/abp/desktop-settings/general-default-language-multiple.jpg)

##### Behaviour
- When there is more than one language filter list in the table, the  [Change language](#change-language) element turns into a bin icon. All filter lists are removable.
- When there is only one language filter list in the table, the remaining filter list is not removable, and the user is only allowed to  [Change language](#change-language) the filter list - this triggers the [Language modal window](#language-drop-down).

##### Removing a language
To remove a language from the table, click the bin icon in the language row. 

#### Language filter list - empty state
![](/res/abp/desktop-settings/general-tab-language-empty.jpg)

##### Text
`You don't have any language-specific filters.`

##### Conditions
When all language filter lists have been removed from the extension.

Note: it is only possible to remove all language filter lists in [Remove filter list subscriptions](#remove-filter-list-subscriptions) in the Advanced tab.


#### Smart Language Filter

Design: [https://eyeogmbh.invisionapp.com/share/AYVJBL68QWU#/screens](https://eyeogmbh.invisionapp.com/share/AYVJBL68QWU#/screens)

By default, [Smart Language Filters](/spec/abp/filter-list-activation.md) feature will be turned off. However, user may have the option to enable it by un-checking the checkbox. 

#### Exceptions

The Smart Language Filter feature is not available for Opera users. For Opera users, the [filter list activation checkbox](#filter-list-activation-checkbox) will be hidden until a solution is found for [an issue](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/issues/960) that makes us unable to detect the implicit web page language for this browser.

The Smart Language Filter feature is not available for `DE` and `FR` locales.

##### Filter List Activation Checkbox

|#|Label|
|-|-----|
|1.|`Notify me of available language filter lists`|


##### Filter List Activation tooltip

`Adblock Plus relies on filter lists to block ads in different languages.`

### Error state

When there is a failure of an action within the page, i.e. if a filter list download fails, display the following error message `Uh oh! Something went wrong. Please try again.`.

This will appear as a banner that will animate from the top down for 3 seconds.

![](/res/abp/desktop-settings/general-tab-error.jpg)
